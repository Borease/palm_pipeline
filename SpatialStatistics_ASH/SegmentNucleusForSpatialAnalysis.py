### Segment the nucleus for K-Ripley analysis
# Written by Herve Marie-Nelly and modified by Anders Sejr Hansen
# Assumes the in the folder one level up there is a single MAT file with PALM localizations stored in a varaible named "nuclear_PALM_locs". The code will then plot the detections and ask the user to segment the nucleus. To segment it, draw red line around the nucleus in the right-hand subplot used the right-hand mouse button. Once you have finished, click "x" on the keyboard

import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import os, fnmatch
import scipy.io as io
from matplotlib.widgets import LassoSelector
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
import triangle
import triangle.plot as tplot


def point_in_poly(x,y,poly):

    n = len(poly)
    inside = False

    p1x,p1y = poly[0]
    for i in range(n+1):
        p2x,p2y = poly[i % n]
        if y > min(p1y,p2y):
            if y <= max(p1y,p2y):
                if x <= max(p1x,p2x):
                    if p1y != p2y:
                        xints = (y-p1y)*(p2x-p1x)/(p2y-p1y)+p1x
                    if p1x == p2x or x <= xints:
                        inside = not inside
        p1x,p1y = p2x,p2y

    return inside

def poly_in_poly(poly_a, poly_b):

    inside = True
    for xy in poly_a:
        x = xy[0]
        y = xy[1]
        test = inside and point_in_poly(x,y, poly_b)
        inside = test
    return inside

def approx_area(poly,):

    x_min = poly[:,0].min()
    x_max = poly[:,0].max()
    y_min = poly[:,1].min()
    y_max = poly[:,1].max()

    area = (x_max - x_min) * (y_max - y_min)
    return area
class my_lasso_selector():
    """
    Lasso custom selector
    """

    def __init__(self, im_ax, scatt_ax, xy, x, y, output_res, output_dir):
        self.im_ax = im_ax
        self.scatt_ax = scatt_ax
        self.lp = dict(color='red', linestyle='-', linewidth=10, alpha=0.9)
        self.lasso = LassoSelector(self.im_ax, self.onselect, lineprops=self.lp)
        self.patches = set()
        self.color = 'r'
        self.color_nuc = 'b'
        self.output_res = output_res
        self.output_dir = output_dir
        try:
            os.mkdir(self.output_dir)
        except OSError:
            print "output folder already created"
        self.triangle_file = os.path.join(self.output_dir, 'triangles.txt')
        self.rectangle_file = os.path.join(self.output_dir, 'rectangles.txt')
        self.localisation_file = os.path.join(self.output_dir, 'localisations.csv')
        self.summary_file = os.path.join(self.output_dir, 'summary_all.csv')
        self.x = x
        self.y = y
        self.xy = xy
        self.out_tri = dict.fromkeys(['segments', 'vertices'])

        print "Start up!!"

    def onselect(self, verts):
        polygon = Polygon(verts, closed=True)
        print "len polygon  = ", len(verts)
        verts_np = np.array(verts, dtype=np.int32)
        print verts_np.shape
        self.coord = verts_np

        if len(verts) > 20:
            self.patches.add(polygon)
            self.p = PatchCollection([polygon], edgecolors='k', facecolors=self.color, alpha=0.4, picker=5)
            self.im_ax.add_collection(self.p, )
        self.poly = polygon
        self.verts = verts
        # self.update_scatt_axis()
        plt.draw()

    def on_key(self, event):
        if event.key == 'x':
            print " X pressed"
            self.curate_polygons()
            self.update_scatt_axis()
    def onpick(self, event):
        p = event.artist
        p.remove()
        self.scatt_ax.cla()
        plt.draw()

        # print "remove polygon:", p


    def curate_polygons(self, ):
        col = self.im_ax.collections
        list_poly = []
        list_area = []
        n_poly = len(col)
        print "################################"
        print "n poly = ", n_poly
        for p in col:
            vertices = p.get_paths()[0].vertices
            print "vertices shape = ", vertices.shape
            vertices_np = np.float32(vertices)
            if len(vertices) > 20:
                poly = Polygon(vertices, closed=True)
                print "poly = ", poly
                list_poly.append(vertices_np)
                area = approx_area(vertices_np)
                list_area.append(area)
        list_area = np.array(list_area)
        id_max = np.argmax(list_area)
        self.nucleus_poly = list_poly.pop(id_max)
        self.nucleoli_poly = []

        for p in list_poly:
            if poly_in_poly(list(p), list(self.nucleus_poly)):
                self.nucleoli_poly.append(p)
        print "n nucleoli = ", len(self.nucleoli_poly)
        print "################################"

    def update_scatt_axis(self, ):
        print "verts len =", len(self.nucleus_poly)
        sub_sample_fact = 10 # modification herve
        print "len sub verts = ", len(self.nucleus_poly[::sub_sample_fact])
        self.scatt_ax.cla()
        self.nm_verts = [(e[0] , e[1] ) for e in self.nucleus_poly[::sub_sample_fact]]
        self.x_nm_verts = np.array([e[0] for e in self.nm_verts], dtype=np.float32)
        self.y_nm_verts = np.array([e[1] for e in self.nm_verts], dtype=np.float32)

        self.nm_path = matplotlib.path.Path(self.nm_verts, _interpolation_steps=2, closed=True)
        self.id_ok = self.nm_path.contains_points(self.xy, radius=1)
        print "id_ok = ", self.id_ok
        self.scatt_ax.plot(self.x[self.id_ok], self.y[self.id_ok], '.r', markersize=1.)

        self.ymin = self.y_nm_verts.min()
        self.ymax = self.y_nm_verts.max()
        self.xmin = self.x_nm_verts.min()
        self.xmax = self.x_nm_verts.max()

        lcontour = []

        lcontour.append(np.array([self.xmin, self.ymin]))
        lcontour.append(np.array([self.xmin, self.ymax]))
        lcontour.append(np.array([self.xmax, self.ymax]))
        lcontour.append(np.array([self.xmax, self.ymin]))

        lcontour.extend(self.nm_verts)
        vertices = np.ascontiguousarray(lcontour, dtype=np.float64)
        self.out_tri['vertices'] = vertices

        lsegments = []
        for ele in range(0, 3):  # rectangular box around nucleus
            lsegments.append([ele, ele + 1])
        lsegments.append([3, 0])
        for ele in range(4, len(self.x_nm_verts) + 3):  # nucleus
            lsegments.append([ele, ele + 1])
        lsegments.append([len(self.nm_verts) + 3, 4])

        sub_sample_nucleolus = 1

        for nucleolus in self.nucleoli_poly: # nucleoli
            n_ele_contour = len(lcontour)
            tmp_nm_verts = [(e[0] , e[1] ) for e in nucleolus[::sub_sample_nucleolus]]
            tmp_x_nm_verts = np.array([e[0] for e in tmp_nm_verts], dtype=np.float32)
            tmp_y_nm_verts = np.array([e[1] for e in tmp_nm_verts], dtype=np.float32)

            for ele in range(n_ele_contour, len(tmp_x_nm_verts) + n_ele_contour - 1):  # nucleus
                lsegments.append([ele, ele + 1])
            lsegments.append([len(tmp_x_nm_verts) + n_ele_contour - 1, n_ele_contour])
            lcontour.extend(tmp_nm_verts)

        vertices = np.ascontiguousarray(lcontour, dtype=np.float64)
        self.out_tri['vertices'] = vertices

        segments = np.ascontiguousarray(lsegments, dtype=np.int32)
        self.out_tri['segments'] = segments
        # center of mass of detections inside nucleus


        c_of_mass_x = np.nanmean(self.x[self.id_ok])
        c_of_mass_y = np.nanmean(self.y[self.id_ok])
        self.im_ax.plot(c_of_mass_x, c_of_mass_y, '+r', markersize=10)
        test_center_ok = True
        for nucleolus in self.nucleoli_poly:
            print "nuc x min = ", nucleolus[:, 0].min()
            print "nuc x max = ", nucleolus[:, 0].max()
            print "nuc y min = ", nucleolus[:, 1].min()
            print "nuc y max = ", nucleolus[:, 1].max()
            print " center of mass = ", c_of_mass_x, c_of_mass_y
            test_center_ok *= not point_in_poly(c_of_mass_x, c_of_mass_y, nucleolus)
        print " test center of mass ok = ", test_center_ok

        # define the inside the nucleus as a hole
        if test_center_ok:
            print "center of mass in nucleus!"
            # holes = np.ascontiguousarray([c_of_mass_y, c_of_mass_x], dtype=np.float64)
            holes = np.ascontiguousarray([c_of_mass_x, c_of_mass_y], dtype=np.float64) # modification herve
        else:
            print "center of mass not in nucleus"
            holes = np.ascontiguousarray([tmp_x_nm_verts[0] + 5, tmp_y_nm_verts[0] + 5], dtype=np.float64)
        print "holes =", holes
        self.out_tri['holes'] = holes

        # self.t = triangle.triangulate(self.out_tri, 'pci')
        self.t = triangle.triangulate(self.out_tri, 'pc')
        triangle.plot.plot(self.scatt_ax, **self.t)
        self.export_results()
        plt.draw()

    def export_results(self,):
        print "writing triangulation files ..."
        h = open(self.triangle_file, 'w')
        dat_triangulation = self.t
        vertices = dat_triangulation['vertices']
        triangles = dat_triangulation['triangles']
        # h.write("%f,%f,%f,%f,%f,%f\n" % (0.0, 0.0, 0.0, 0.0, 0.0, 0.0))
        for tri in triangles:
            v0x, v0y = np.float32(vertices[tri[0]])
            v1x, v1y = np.float32(vertices[tri[1]])
            v2x, v2y = np.float32(vertices[tri[2]])
            h.write("%f,%f,%f,%f,%f,%f\n" % (v0x, v0y, v1x, v1y, v2x, v2y))
        h.close()
        h = open(self.rectangle_file, 'w')
        h.write("%d,%d,%d,%d\n" % (self.xmin -1, self.ymin - 1, self.xmax + 1, self.ymax + 1))
        h.close()

        h = open(self.localisation_file, 'w')
        for x_, y_ in zip(self.x[self.id_ok], self.y[self.id_ok]):
            h.write("%f,%f\n" % (x_, y_))
            # h.write("%f,%f\n" % (y, x))
        h.close()
        h = open(self.summary_file, 'w')
        h.write("%s,%s,%s\n" % (self.rectangle_file, self.triangle_file, self.localisation_file))
        h.close()

        print "done!"

if __name__ == "__main__":
    data_dir = os.getcwd()
    MAT_dir = MAT_dir = os.path.dirname(os.getcwd())
    output_folder = os.path.join(data_dir, 'output_folder')
    # EDIT by Anders: Need to automatically find MAT file name:
    FileList = os.listdir(MAT_dir) # list of files in dir with MAT-file
    pattern = "*.mat"
    for entry in FileList:
        if fnmatch.fnmatch(entry, pattern):
                im_file_mat = entry
    #im_file_mat = '20180711_mESC_C59_D2_e10-3xHA-Halo-mCTCF_500nM_PA-JF646_40-500mW-633_rising-405_15x_25msCam_cell01Nucleus1.mat'
    mat_data = io.loadmat(os.path.join(MAT_dir, im_file_mat))
    x = mat_data['nuclear_PALM_locs'][:,1]
    y = mat_data['nuclear_PALM_locs'][:,0]
    #x = mat_data['vutarax'].ravel()
    #y = mat_data['vutaray'].ravel()
    xy = [(i,j) for i, j in zip(x, y)]

    output_res = 30
    s_x_min = x.min()
    s_x_max = x.max()
    s_y_min = y.min()
    s_y_max = y.max()
    s_xedges = np.arange(int(s_x_min), int(s_x_max), output_res)
    s_yedges = np.arange(int(s_y_min), int(s_y_max), output_res)
    sub_H, s_xedges, s_yedges = np.histogram2d(y, x, bins=(s_yedges, s_xedges))

    fig = plt.figure()
    ax1 = plt.subplot(1, 2, 1, aspect='equal')
    im_obj = ax1.imshow(sub_H, interpolation='nearest', origin='bottom', extent=[s_x_min, s_x_max,
                                                                                 s_y_min, s_y_max])
    ax2 = plt.subplot(1, 2, 2, aspect='equal')
    scat_obj = ax2.plot(x, y, '.r', markersize=0.5)
    myL = my_lasso_selector(ax1, ax2, xy, x, y, output_res, output_folder)
    im_obj.set_clim([0, sub_H.max() * 0.09])
    fig.canvas.mpl_connect('pick_event', myL.onpick)
    fig.canvas.mpl_connect('key_press_event', myL.on_key)
    plt.show()
