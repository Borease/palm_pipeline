library(ads)
# Run Ripley Analysis using the ads library.
# Start the clock!
ptm <- proc.time()

data_dir<- paste(getwd(), "output_folder/", sep="/") #automatically identify the data directory
print(data_dir)
s_file_R<-paste(data_dir, "summary_all.csv", sep="")
all_files_R<-read.csv(s_file_R, header=FALSE)
print(all_files_R)
for(id_nuc in 1:1){
    print(paste("id_file = ", id_nuc, sep=" "))

    f_R<-all_files_R[id_nuc,]
    print(toString(f_R[,2]))
    rect_R<-read.csv(toString(f_R[,1]), header=FALSE)
    tri_R<-read.csv(toString(f_R[,2]), header=FALSE)
    points_R<-read.csv(toString(f_R[,3]), header=FALSE)


    print("Loading Triangles...")
    tR<-c()
    for(i in 1:dim(tri_R)[1]){
        tR<-rbind(tR, c(as.numeric(tri_R[i,])))
    }

    wrR<-swin(c(as.numeric(rect_R)))
    wsR<-swin(wrR, tR)
    print("running K-Ripley")
    swR<-spp(x=c(as.numeric(points_R[,1])), y=c(as.numeric(points_R[,2])), win=wrR, tri=tR)
    # plot(swR)
    # points(points_R)

    print("Data loaded.")
    max_dist_k<-1050.
    n_simu<-0
    print("Computing K-Ripley, PC, L ...")
    kswrtR<-kfun(swR, max_dist_k, 1, n_simu)



    kRobs<-kswrtR$k$obs / max(kswrtR$k$obs)
    kRtheo<-kswrtR$k$theo / max(kswrtR$k$theo)

    lRobs<-kswrtR$l$obs / max(kswrtR$l$obs)
    lRtheo<-kswrtR$l$theo / max(kswrtR$l$theo)

    print("Save results...")
    out_file<-paste("res_ripley", id_nuc, ".RData", sep="_")
    print(paste("id_file = ", id_nuc, sep=" "))
    save(kswrtR, file = out_file)
}

# Stop the clock
proc.time() - ptm
