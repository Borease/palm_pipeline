PALM Pipeline
--------------------------
written and maintained by Anders Sejr Hansen

# PALM_Pipeline
This repository contains Matlab code for processing PALM movies where
the localization step has already been performed (using code available
at ![GitLab](https://gitlab.com/tjian-darzacq-lab/SPT_LocAndTrack)).
Most steps are performed by "DriftCorrectMergeBlinks.m", which calls
various subfunctions.


## Quick tutorial: going through each step

1. **Step 1**: Obtain PALM data 
   1. Perform localization using the
      ![MTT Algorithm](https://gitlab.com/tjian-darzacq-lab/SPT_LocAndTrack)
      or your algorithm of choice. But will need to be in the above format to
   be read by this pipeline
2. **Step 2**: Perform Drift-correction
   1. Run `DriftCorrectMergeBlinks.m`
   2. Drift-correction is performed using a custom-modified version of
      the BaSDI algorithm (see function
      `IterativeBaSDI_DriftCorrect.m`).
   3. Adjust these parameters: `ParamStruct.FramesBin`;
      `ParamStruct.PixelBin`; `ParamStruct.Iterations`;
3. **Step 3**: Merge photo-blinkers and multiple appearances
   1. Often a single molecule will appear in many frames and blink. We need to
   merge all of these appearences into a single localization.
   2. This is achieved using Nearest Neighbor tracking. Default
      parameters: `ParamStruct.MaxLinkingDistance = 75`; (nm);
      `ParamStruct.MaxGapClosing = 2`;
4. **Step 4**: Segment nuclei
   1. To improve the robustness of the drift-correction algorithm, we
   recommend recording movies of 3-5 nuclei at a time. This means they
   have to be segmented out.
   2. After running the steps above, Matlab will reconstruct images
   and ask the user to manually segment out the nuclei.
5. **Step 5**: Saving and QC
   1. Matlab will save everything to a directory for each nucleus and
      also add scripts for running downstream analysis to each
      directory. 
   2. Matlab will plot and save a figure with relevant summary
      statistics for each nucleus. 
6. **Step 6**: Run downstream analysis
   1. To run K-Ripley analysis (using Python and R code written by
      Herve Marie-Nelly), please follow the instructions in
      `HowToRunPythonR_RipleyCode.rtf`. You will need to install
      several Python and R libraries.
   2. To run Cluster Identification code (from Rubin-Delanchy et al.),
       please see the relevant ReadMe file here as well
7. **Step 7**: Process outputs
   1. The outputs from K-Ripley and Bayesian Cluster Identification
      can be processed using `PLOT_K_L_g_Ripley.m` and `ProcessBayesClusterData_v5.m`.



#### Issues
This code was tested with Matlab 2014b on a Mac. Some functionalities
may not work in newer or older versions. 

## License
These programs are released under the GNU General Public License version 3 or upper (GPLv3+).


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.


## Acknowledgements
This project uses several tools from other people. In order for
appearance:

I would like to thank Ji Yu for helpful discussions and help with
debugging. His algorithm, BaSDI, was previously published (the version
used here has been modified and is used in an iterative manner):

    Elmokadem, A., and Yu, J. (2015). Optimal Drift Correction for
	Superresolution Localization Microscopy with Bayesian Inference.
	Biophys. J. 109, 1772–1780.

I would also like to thank Jean-Yves Tinevez for making SimpleTracker
freely available and very easy to use. Moreover, as I was modifying
SimpleTracker, I would also like to thank Jean-Yves Tinevez for help
in doing so and for discussions. SimpleTracker can be found on the ![Matlab FileExchange](https://www.mathworks.com/matlabcentral/fileexchange/34040-simple-tracker)

I would also like to thank Herve Marie-Nelly for kindly sharing and
helping to debug his code for running K-Ripley analysis in R as well
as his Python code for the neccesary triangulation. Herve's K-Ripley
code was first reported in:

	Boehning, M., Dugast-Darzacq, C., Rankovic, M., Hansen, A.S., Yu, T.
	, Marie-Nelly, H., McSwiggen, D.T., Kokic, G., Dailey, G.M., Cramer, P., et al. (2018).
	RNA polymerase II clustering through carboxy-terminal domain phase separation. Nat. Struct. Mol. Biol.

The K-Ripley code makes use of the ads library which is available on
![CRAN](https://cran.r-project.org/web/packages/ads/index.html).

The Bayesian Cluster Identification algorithm used is here is the one
published by Ruben-Delanchy et al. and it is used here without any
modification and using the default parameters:

	Rubin-Delanchy, P., Burn, G.L., Griffié, J., Williamson, D.J., Heard,N.A., Cope, A.P., and Owen, D.M. (2015).
	Bayesian cluster identification in single-molecule localization microscopy data. Nat. Methods 12, 1072–1076.

Finally, for visualizing PALM reconstructions, we highly recommend
ViSP by Mohamed El Beheiry and Maxime Dahan:

	Beheiry, M. El, and Dahan, M. (2013).
	ViSP: representing single-particle localizations in three dimensions. Nat. Methods 10, 689.





