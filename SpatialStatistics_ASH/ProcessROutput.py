import rpy2.robjects as robjects
import numpy as np
import matplotlib
matplotlib.use('Qt5Agg')
import matplotlib.pyplot as plt
import os
from scipy.stats import sem as sem
from scipy.ndimage import gaussian_filter
from scipy.io import savemat


if __name__ == "__main__":
    data_dir = os.getcwd()
    r_res_file = 'res_ripley_1_.RData'
    mat_res_file = r_res_file.replace('.RData', '.mat')
    f = os.path.join(data_dir, r_res_file)
    robjects.r['load'](f)
    obs_G = np.array(robjects.r["kswrtR"][2][0], dtype=np.float32)
    theo_G = np.array(robjects.r["kswrtR"][2][1], dtype=np.float32)
    obs_N = np.array(robjects.r["kswrtR"][3][0], dtype=np.float32)
    theo_N = np.array(robjects.r["kswrtR"][3][1], dtype=np.float32)
    obs_K = np.array(robjects.r["kswrtR"][4][0], dtype=np.float32)
    theo_K = np.array(robjects.r["kswrtR"][4][1], dtype=np.float32)
    obs_L = np.array(robjects.r["kswrtR"][5][0], dtype=np.float32)
    theo_L = np.array(robjects.r["kswrtR"][5][1], dtype=np.float32)

    out = dict()
    out['kripley'] = obs_K
    out['l_func'] = obs_L
    out['n_func'] = obs_N
    out['g_func'] = obs_G

    savemat(mat_res_file, out)
