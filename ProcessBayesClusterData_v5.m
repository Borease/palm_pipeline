%   ProcessBayesClusterData_v5.m   
%   Anders Sejr Hansen, Sep 2018
clear; clc; close all;

%   Process output from the Bayesian Clusterin Analysis script that you
%   have to run in R Studio. 
%   Need to do the following:
%   Go to the folder "DataFolder" and read in the files: "nmols.txt" and
%   "radii.txt". 
%   Make a list of all the subfolders of DataFolder and then go to each
%   subfolder and read in "summary.txt" and "r_vs_thresh.txt". 

%   UPDATE July 2016 - version 2
%   Don't delete all of the many txt files - save the one with the max r,T
%   combination for plotting the clustering yourself. 

%   UPDATE July 10, 2016  - version 3
%   There is an annoying numbering issue due to the way things are ordered. 
%   The first square is "1", the second square "10", the third "11", the
%   twelth is "2" and so on. So when when you use MatLab to make a list of
%   directories, the messes up the numbering, which messes up the location
%   of each square relative to the parent workspace. So pre-define squares
%   from workspace. This has the limitation, that the code will only run if
%   all of the squares defined were successfully analyzed. So cannot stop
%   the run early if you get impatient. 

%   UPDATE August 9, 2016 - version 4
%   It was getting annoying to input one workspace at a time so re-wrote
%   the script to automatically go through a folder with subfolders where
%   each cell is in "cell1"... "cell11" subfolders and then process each
%   one, one at a time. 

%   UPDATE SEPTEMBER 20, 2018 - version 5
%   Make it work with 2018 PALM pipeline. 

%%%% DO YOU WANT TO SAVE-APPEND ALL THE SQUARE INFORMATION?
save_append_val = 1;
OneOrManyWorkspaces = 2; %set to 1 if single workspace; set to 2 if full folder.

ClusterNumberFile = 'nmols.txt';
ClusterRadiusFile = 'radii.txt';
ProbDensity2DFile = 'r_vs_thresh.txt';
StatsSummaryFile = 'summary.txt';

% Add plotSpread to path
dir0=pwd; addpath(genpath([dir0 filesep '.' filesep 'plotSpread']))


%%%%%%%%%%%%%%%%%% SINGLE WORKSPACE OR FULL FOLDER ??? %%%%%%%%%%%%%%%%%%
if OneOrManyWorkspaces == 1
    %Where to find workspaces
    %InputPath = '/Users/anderssejrhansen/Dropbox/MatLab/Lab/Microscopy/SingleParticleTracking/PALM/PALM_reconstruction_data/20160831_mESC_C59_Halo-mCTCF_PBS_500nM_PA-JF549/cell9/';
    %Workspaces = {'20160831_mESC_C59_Halo-mCTCF_500nM_PA-JF549_40-500mW-549_rising-405_25msCam_cell09_Tracked.mat_PALM_localizations'};
elseif OneOrManyWorkspaces == 2
    % give an input path which contains subfolders, where each subfolder is
    % named e.g. "cell1", "cell2" etc. 
    InputPath = '/Users/AndersSejrHansen/Dropbox/MatLab/Lab/Microscopy/SingleParticleTracking/PALM/PALM_reconstruction_data/20180907_C59_D2_PA_JF549_PALM/Analyzed/20180907_C59_D2_PA_JF549_PALM/';
    SubFolderIdentifier = 'Nucleus';
    
    %%%%% FIND ALL SUB-FOLDERS %%%%%
    % Find all the SubFolders called "cell...":
    DirFolders = dir(InputPath);
    FolderList = {''};
    iter = 1;
    for i=1:size(DirFolders,1)
        %Check if it is the right name
        if strfind(DirFolders(i).name, SubFolderIdentifier) > 0 
            FolderList{iter} = DirFolders(i).name;
            iter = iter + 1;
        end
    end
    
    %%%%% FIND ALL WORKSPACES IN SUB-FOLDERS %%%%%
    Workspaces = {''};
    % go through each subfolder and search for matlab workspace which ends
    % with '_PALM_localizations.mat':
    for i = 1:length(FolderList)
        DirFiles = dir([InputPath, FolderList{i}]);
        FoundWorkspace = 0;
        for j=1:length(DirFiles)
            if strfind(DirFiles(j).name, '.mat') > 1 
                Workspaces{i} = DirFiles(j).name;
                FoundWorkspace = FoundWorkspace + 1;
                if FoundWorkspace > 1
                    Error(['There were two MATLAB workspaces in folder:', FolderList{i}, '; Need there to be one and only one. Sort it out']);
                end
            end
        end
        if FoundWorkspace == 0
            Error(['There were no MATLAB workspaces in folder:', FolderList{i}, '; Need there to be one and only one. Sort it out']);
        end
    end
end

%Loop over each cell
for WorkIter=1:length(Workspaces);
    tic;
    close all;
    
    if OneOrManyWorkspaces == 1
        EffInputPath = InputPath;
    elseif OneOrManyWorkspaces == 2
        EffInputPath = [InputPath, FolderList{WorkIter}, '/'];
        Workspaces{WorkIter} = Workspaces{WorkIter}(1:end-4); %cut off the '.mat' suffix
        disp('============================================================');
        disp('============================================================');
        disp(['Load in Bayes nucleus ', num2str(WorkIter), ' of ', num2str(length(Workspaces)), ' nuclei.']);
    end
    
    
    %%%%%%%%%%%%%%%%%%%%% Read in global statistics %%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   Read in overall stats:
    ParsPerCluster = csvread([EffInputPath, 'BayesPALM_clustering/DataFolder/', ClusterNumberFile]);
    ClusterRadii = csvread([EffInputPath, 'BayesPALM_clustering/DataFolder/', ClusterRadiusFile]);

    %%%%%%%%%%%%%%%%%%% Read in subfolder statistics %%%%%%%%%%%%%%%%%%%%%%%%%%
    % load in the parent workspace:
    load([EffInputPath, Workspaces{WorkIter}, '.mat']);

    %define variables
    mean_NumberOfClusters = zeros(1,size(Square_parameters,1));
    mean_PercentInCluster = zeros(1,size(Square_parameters,1));
    mean_NumberInCluster = zeros(1,size(Square_parameters,1));
    mean_ClusterRadius = zeros(1,size(Square_parameters,1));
    %store r vs. T in a cell array:
    r_vs_T_cell = cell(1,size(Square_parameters,1));
    % store the best r,T combination for each square:
    best_r_vs_T_idx = zeros(size(Square_parameters,1),2);
    %%%% define variable for saving clustering:
    ClusteredSquareParticles_local = cell(1,size(Square_parameters,1));
    ClusteredSquareParticles_global = cell(1,size(Square_parameters,1));
    %   ClusteredSquareParticles organization:
    %   each element is a matrix where:
    %       1st column: x-val
    %       2nd column: y-val
    %       3rd column: Loc Error
    %       4th column: cluster assignment (1 = background; n>1: cluster n)

    %loop through each subfolder:
    for SquareNumb=1:size(Square_parameters,1)
        %   Now load file with the summary statistics:
        fid   = fopen([EffInputPath, 'BayesPALM_clustering/DataFolder/', num2str(SquareNumb), '/', StatsSummaryFile]);
        lines = textscan(fid, '%s');
        fclose(fid);

        %   OK, this is pretty annoying, but no way around it. Have to
        %   read in the terrible format of the summary.txt file:
        SplitCell = lines{1,1};
        %   Number of clusters is in line 7:
        idx = regexp(SplitCell{7},'\d');
        mean_NumberOfClusters(1,SquareNumb) = str2num(SplitCell{7}(min(idx):max(idx)));
        %   Cluster Percentage is in line 11:
        idx = regexp(SplitCell{11},'\d');
        mean_PercentInCluster(1,SquareNumb) = str2num(SplitCell{11}(min(idx):max(idx)));
        %   Number of molecules per cluster is in line 18:
        idx = regexp(SplitCell{18},'\d');
        mean_NumberInCluster(1,SquareNumb) = str2num(SplitCell{18}(min(idx):max(idx)));
        %   Mean radius per cluster is in line 21:
        idx = regexp(SplitCell{21},'\d');
        mean_ClusterRadius(1,SquareNumb) = str2num(SplitCell{21}(min(idx):max(idx)));


        %Next up, read in the r_vs_treshold file:
        r_vs_T = csvread([EffInputPath, 'BayesPALM_clustering/DataFolder/', num2str(SquareNumb), '/', ProbDensity2DFile]);
        r_vs_T_cell{1,SquareNumb} = r_vs_T;

        % Find the best fit r,T combo for the square:
        temp_r_vs_T = abs(r_vs_T(2:end, 2:end)); % remove r,T vals
        [val, idx] = min(temp_r_vs_T(:));
        [idx_row, idx_col] = ind2sub(size(temp_r_vs_T),idx);
        best_r_vs_T_idx(SquareNumb,:) = [idx_row, idx_col];

    end

    %%%%% Average the 2-D probability density map and extract x,y, vectors:
    r_vector = r_vs_T(1,2:end);
    T_vector = r_vs_T(2:end,1)';
    % Average over the density in each square:
    mean_r_vs_T = zeros(length(T_vector), length(r_vector));
    for i=1:length(r_vs_T_cell)
        mean_r_vs_T = mean_r_vs_T + r_vs_T_cell{i}(2:end,2:end);
    end
    %Normalize by length:
    mean_r_vs_T = mean_r_vs_T ./ length(r_vs_T_cell);

    % Figure out the best r,T combo for each square:
    best_r_vs_T = zeros(size(best_r_vs_T_idx,1), 2); %1st column: T; 2nd column: r;
    for i=1:size(best_r_vs_T_idx,1)
        best_r_vs_T(i,:) = [T_vector(best_r_vs_T_idx(i,1)), r_vector(best_r_vs_T_idx(i,2))];
    end

    % Find the best global r,t:
    [val, idx] = min(abs(mean_r_vs_T(:)));
    [idx_row, idx_col] = ind2sub(size(mean_r_vs_T),idx);
    global_r_T = [T_vector(idx_row), r_vector(idx_col)];

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%% DELETING FILES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %   OK, now you have found the best global_r_T combo and the best r,t combo
    %   for each individual square. So delete all labels files except for the
    %   best ones. 
    %   So go back into each folder and delete all txt files except for these
    %   two:
    %loop through each subfolder:
    for SquareNumb=1:size(Square_parameters,1)       
        % remove most files in the labels folder - it takes up around 50 MB per subfolder
        curr_dir = pwd;
        % go to the labels subfolder
        cd([EffInputPath, 'BayesPALM_clustering/DataFolder/', num2str(SquareNumb), '/labels'])

        %%% find all txt files in the current directory:
        f=dir('*.txt');
        f={f.name};
        % search for the file name with the best square param:
        local_square_name = ['clusterscale', num2str(best_r_vs_T(SquareNumb,2)), ' thresh', num2str(best_r_vs_T(SquareNumb,1)), 'labels.txt'];
        global_square_name = ['clusterscale', num2str(global_r_T(2)), ' thresh', num2str(global_r_T(1)), 'labels.txt'];

        % now remove these two txt files from the list:
        n=find(strcmp(f,local_square_name));
        if ~isempty(n)
            f{n}=[];
        end
        n=find(strcmp(f,global_square_name));
        if ~isempty(n)
            f{n}=[];
        end
        % now delete all the other files:
        for k=1:numel(f);
            delete(f{k});
        end


        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%% SAVING CLUSTER DATA FOR EACH SQUARE %%%%%%%%%%%%%%%%%%%%
        % go back to the directory with the data.txt file
        cd([EffInputPath, 'BayesPALM_clustering/DataFolder/', num2str(SquareNumb)])
        %   The next part is a little complicated, but here is what to do:
        %   1. go through each square
        %   2. load in the "data.txt" file
        %   3. load in the best local and global cluster assignment
        %   4. save the data so that each par is associated with a cluster

        %   ClusteredSquareParticles organization:
        %   each element is a matrix where:
        %       1st column: x-val
        %       2nd column: y-val
        %       3rd column: Loc Error
        %       4th column: cluster assignment (1 = background; n>1: cluster n)

        % read in data.txt - avoid the first row since it contains text
        DataFile = csvread('data.txt',1);

        %   OK, now for the fourth column:
        %   assign clusters for global and local:

        %%%%%%%%%%%%% SAVING LOCAL BEST CLUSTERING FOR SQUARE %%%%%%%%%%%%%
        % LOCAL FIRST:
        local_clusters = csvread(['./labels/', local_square_name]);
        background_assignment = ones(length(local_clusters), 1);

        %   unique assigments: background
        %   >duplicates: clusters
        UniqueAssigns = unique(local_clusters);
        % find non-uniques (>duplicates)
        duplicates = histc(local_clusters, UniqueAssigns);
        thres = duplicates>1;
        clusterVals = find(thres>0);
        % now go through and find the index of each ClusterVal
        for k=1:length(clusterVals)
            curr_idx = find(local_clusters == clusterVals(k));
            background_assignment(curr_idx) = k+1;
        end
        % OK, now save the clustering assignment to each particle:
        ClusteredSquareParticles_local{1,SquareNumb} = [DataFile, background_assignment];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        %%%%%%%%%%%% SAVING GLOBAL BEST CLUSTERING FOR SQUARE %%%%%%%%%%%%%
        %%%% REPEAT FOR GLOBAL:
        global_clusters = csvread(['./labels/', global_square_name]);
        background_assignment = ones(length(global_clusters), 1);

        %   unique assigments: background
        %   >duplicates: clusters
        UniqueAssigns = unique(global_clusters);
        % find non-uniques (>duplicates)
        duplicates = histc(global_clusters, UniqueAssigns);
        thres = duplicates>1;
        clusterVals = find(thres>0);
        % now go through and find the index of each ClusterVal
        for k=1:length(clusterVals)
            curr_idx = find(global_clusters == clusterVals(k));
            background_assignment(curr_idx) = k+1;
        end
        % OK, now save the clustering assignment to each particle:
        ClusteredSquareParticles_global{1,SquareNumb} = [DataFile, background_assignment];
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        cd(curr_dir);

    end
    toc; 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%%%%%%%%%%%%%%%%%%%%%%%% PLOT ALL OF THE OUTPUT %%%%%%%%%%%%%%%%%%%%%%%%%%
    figure1 = figure('position',[100 100 1000 800]); %[x y width height]
    %   Plot ImageSC of Posterior Probability
    %   Plot Bar graph with means and errors
    %   Plot histogram for Number of molecules
    %   Plot histogram for Cluster Radius

    %%%%%%%%%% PLOT THE POSTERIOR PROBABILITY HEATMAP %%%%%%%%%%%%%%%%
    subplot1 = subplot(4,4,[1,2,5,6]);
    hold on;
    box(subplot1,'on');
    imagesc(r_vector, T_vector, mean_r_vs_T);
    ylim([min(T_vector) max(T_vector)]);
    xlim([min(r_vector) max(r_vector)]);
    caxis([min(min(mean_r_vs_T)) max(max(mean_r_vs_T))]) % change colorbar limits
    plot(global_r_T(2), global_r_T(1), 'ko', 'MarkerSize', 12); 
    set(gca, 'XTick', 0:50:200, 'XMinorTick','off');
    set(gca, 'XTickLabel', {'0', '50', '100', '150', '200'});
    set(gca, 'YTick', 0:100:500);
    set(gca, 'YTickLabel', {'0', '100', '200', '300', '400', '500'});
    title({Workspaces{WorkIter}; 'posterior probability heatmap for cluster'}, 'FontSize',9, 'FontName', 'Helvetica');
    xlabel('cluster radius (nm)', 'FontSize',9, 'FontName', 'Helvetica');
    ylabel('cluster threshold (AU)', 'FontSize',9, 'FontName', 'Helvetica');
    colorbar;%
    hold off;

    %%%%%%%%%%%%%%%% PLOT BEESWARM PLOTS WITH SUMMARY STATISTICS %%%%%%%%%%%%%%
    subplot(4,4,3);
    plotSpread(mean_NumberOfClusters'/9, 0.1, {'xp',[]}, {'mean for each square'},5);
    ylim([0 1.4*max(mean_NumberOfClusters)/9]);
    xlim([0.5 1.5]);
    ylabel('# of clusters per um^2', 'FontSize',9, 'FontName', 'Helvetica');
    title('# of cluster per um^2', 'FontSize',9, 'FontName', 'Helvetica');

    subplot(4,4,4);
    plotSpread(mean_NumberInCluster', 0.1, {'xp',[]}, {'mean for each square'},5);
    ylim([0 1.1*max(mean_NumberInCluster)]);
    xlim([0.5 1.5]);
    ylabel('molecules per cluster', 'FontSize',9, 'FontName', 'Helvetica');
    title('CTCF molecules per cluster', 'FontSize',9, 'FontName', 'Helvetica');

    subplot(4,4,7);
    plotSpread(mean_PercentInCluster'/100, 0.1, {'xp',[]}, {'mean for each square'},5);
    ylim([0 1.02]);
    xlim([0.5 1.5]);
    ylabel('fraction of molecules in clusters', 'FontSize',9, 'FontName', 'Helvetica');
    title('fraction of molecules in clusters', 'FontSize',9, 'FontName', 'Helvetica');

    subplot(4,4,8);
    plotSpread(mean_ClusterRadius', 0.1, {'xp',[]}, {'mean for each square'},5);
    ylim([0 1.1*max(mean_ClusterRadius)]);
    xlim([0.5 1.5]);
    ylabel('cluster radius (nm)', 'FontSize',9, 'FontName', 'Helvetica');
    title('CTCF cluster size', 'FontSize',9, 'FontName', 'Helvetica');


    %%%%%%%%%% PLOT HISTOGRAMS OF MOLECULES PER CLUSTER %%%%%%%%%%%%%%%%
    %   Bin all the molecules per cluster vector:
    mols_vector = 0:1:round(1.05*max(ParsPerCluster));
    ParsPerClusterPDF = histc(ParsPerCluster, mols_vector);
    ParsPerClusterPDF = ParsPerClusterPDF ./ sum(ParsPerClusterPDF);
    ParsPerClusterCDF = zeros(1,length(ParsPerClusterPDF));
    for i=2:length(ParsPerClusterCDF)
        ParsPerClusterCDF(1,i) = sum(ParsPerClusterPDF(1,1:i));
    end
    cParsPerClusterCDF = 1-ParsPerClusterCDF;

    % Fit 1-exp and 2-exp to the CDF:
    x_vec = mols_vector;
    yProb = cParsPerClusterCDF;
    %define fitting functions
    f = fittype('exp(-a*x)');
    [OneExp_fit, OneExp_param] = fit(x_vec', yProb', f, 'Lower', 0, 'Upper', 10, 'StartPoint', 0.5);
    f2 = fittype('F*exp(-a*x) + (1-F)*exp(-b*x)');
    [TwoExp_fit, TwoExp_param] = fit(x_vec', yProb', f2, 'Lower', [0 0.02 0], 'Upper', [1 10 0.9], 'StartPoint', [0.9 2 0.02]); 

    %fit for plot
    xFit = 0:0.25:length(cParsPerClusterCDF);
    yFit = exp(-OneExp_fit.a.*xFit);
    yFit2 = TwoExp_fit.F.*exp(-TwoExp_fit.a.*xFit) + (1-TwoExp_fit.F).*exp(-TwoExp_fit.b.*xFit);
    %test summary of fit
    Fit1_text(1) = {'One Exp fit: exp(-k*n)'};
    Fit1_text(2) = {['k = ', num2str(OneExp_fit.a), ' mol^-1 or 1/k = ', num2str(1/OneExp_fit.a), ' molecules']};
    Fit1_text(3) = {'Two Exponential fit: F*exp(-a*n) + (1-F)*exp(-b*n)'};
    Fit1_text(4) = {['F = ', num2str(TwoExp_fit.F)]};
    Fit1_text(5) = {['a = ', num2str(TwoExp_fit.a), ' mol^-1 or 1/a = ', num2str(1/TwoExp_fit.a), ' molecules']};
    Fit1_text(6) = {['b = ', num2str(TwoExp_fit.b), ' mol^-1 or 1/b = ', num2str(1/TwoExp_fit.b), ' molecues']};

    subplot(4,4,[9,10]);
    hold on;
    for i=2:length(mols_vector)
        x1 = mols_vector(1,i-1); x2 = mols_vector(1,i);
        y1 = 0; y2 = ParsPerClusterPDF(i-1);
        patch([x1 x1 x2 x2], [y1 y2 y2 y1],[237/255, 28/255, 36/255],'LineStyle','none');
    end
    text(0.5*prctile(ParsPerCluster, 99), 0.7*max(ParsPerClusterPDF), {'histogram with data from all squares'; ['<molecules per cluster> = ', num2str(mean(ParsPerCluster))]}, 'FontSize',7, 'FontName', 'Helvetica');
    axis([0 prctile(ParsPerCluster, 99) 0 1.05*max(ParsPerClusterPDF)]);
    xlabel('number of molecules per cluster', 'FontSize',9, 'FontName', 'Helvetica');
    ylabel('probability', 'FontSize',9, 'FontName', 'Helvetica');
    title('histogram of CTCF molecules per cluster', 'FontSize',9, 'FontName', 'Helvetica');
    hold off;

    subplot(4,4,[13,14]);
    hold on;
    %plot(mols_vector, cParsPerClusterCDF, '-', 'LineWidth', 2, 'Color', [237/255, 28/255, 36/255]);
    plot(mols_vector, cParsPerClusterCDF, 'ko', 'MarkerSize', 6, 'MarkerFaceColor', 'r');
    plot(xFit+1, yFit, 'k--', 'LineWidth', 1.5);
    plot(xFit+1, yFit2, 'k-', 'LineWidth', 1.5);
    text(0.35*prctile(ParsPerCluster, 99), 0.4, Fit1_text,'HorizontalAlignment','Left', 'FontSize',7, 'FontName', 'Helvetica');
    xlabel('number of molecules per cluster', 'FontSize',9, 'FontName', 'Helvetica');
    ylabel('1-CDF', 'FontSize',9, 'FontName', 'Helvetica');
    title('1-CDF of CTCF molecules per cluster', 'FontSize',9, 'FontName', 'Helvetica');
    legend('data', '1-exp', '2-exp', 'Location', 'NorthEast');
    axis([0 prctile(ParsPerCluster, 99.5) 0.005 1.01]);
    set(gca,'yscale','log');
    hold off;


    %%%%%%%%%% PLOT HISTOGRAMS OF CLUSTER RADII %%%%%%%%%%%%%%%%
    %   Bin all the molecules per cluster vector:
    radius_vector = 0:1:round(1.05*max(ClusterRadii));
    ClusterRadiiPDF = histc(ClusterRadii, radius_vector);
    ClusterRadiiPDF = ClusterRadiiPDF ./ sum(ClusterRadiiPDF);
    ClusterRadiiCDF = zeros(1,length(ClusterRadiiPDF));
    for i=2:length(ClusterRadiiCDF)
        ClusterRadiiCDF(1,i) = sum(ClusterRadiiPDF(1,1:i));
    end
    cClusterRadiiCDF = 1-ClusterRadiiCDF;

    % Fit 1-exp and 2-exp to the CDF:
    x_vec = radius_vector;
    yProb = cClusterRadiiCDF;
    %define fitting functions
    f = fittype('exp(-a*x)');
    [OneExp_fit, OneExp_param] = fit(x_vec', yProb', f, 'Lower', 0, 'Upper', 10, 'StartPoint', 0.5);
    f2 = fittype('F*exp(-a*x) + (1-F)*exp(-b*x)');
    [TwoExp_fit, TwoExp_param] = fit(x_vec', yProb', f2, 'Lower', [0 0.01 0], 'Upper', [1 10 0.9], 'StartPoint', [0.9 2 0.01]); 

    %fit for plot
    xFit = 0:0.25:length(cClusterRadiiCDF);
    yFit = exp(-OneExp_fit.a.*xFit);
    yFit2 = TwoExp_fit.F.*exp(-TwoExp_fit.a.*xFit) + (1-TwoExp_fit.F).*exp(-TwoExp_fit.b.*xFit);
    %test summary of fit
    Fit1_text(1) = {'One Exp fit: exp(-k*r)'};
    Fit1_text(2) = {['k = ', num2str(OneExp_fit.a), ' nm^-1 or 1/k = ', num2str(1/OneExp_fit.a), ' nm']};
    Fit1_text(3) = {'Two Exponential fit: F*exp(-a*r) + (1-F)*exp(-b*r)'};
    Fit1_text(4) = {['F = ', num2str(TwoExp_fit.F)]};
    Fit1_text(5) = {['a = ', num2str(TwoExp_fit.a), ' nm^-1 or 1/a = ', num2str(1/TwoExp_fit.a), ' nm']};
    Fit1_text(6) = {['b = ', num2str(TwoExp_fit.b), ' nm^-1 or 1/b = ', num2str(1/TwoExp_fit.b), ' nm']};

    subplot(4,4,[11,12]);
    hold on;
    for i=2:length(radius_vector)
        x1 = radius_vector(1,i-1); x2 = radius_vector(1,i);
        y1 = 0; y2 = ClusterRadiiPDF(i-1);
        patch([x1 x1 x2 x2], [y1 y2 y2 y1],[237/255, 28/255, 36/255],'LineStyle','none');
    end
    text(0.5*prctile(ClusterRadii, 99), 0.7*max(ClusterRadiiPDF), {'histogram with data from all squares'; ['<cluster radius> = ', num2str(mean(ClusterRadii))]}, 'FontSize',7, 'FontName', 'Helvetica');
    axis([0 prctile(ClusterRadii, 99) 0 1.05*max(ClusterRadiiPDF)]);
    xlabel('cluster radius (nm)', 'FontSize',9, 'FontName', 'Helvetica');
    ylabel('probability', 'FontSize',9, 'FontName', 'Helvetica');
    title('histogram of CTCF cluster radii', 'FontSize',9, 'FontName', 'Helvetica');
    hold off;

    subplot(4,4,[15,16]);
    hold on;
    plot(radius_vector, cClusterRadiiCDF, 'ko', 'MarkerSize', 6, 'MarkerFaceColor', 'r');
    plot(xFit+1, yFit, 'k--', 'LineWidth', 1.5);
    plot(xFit+1, yFit2, 'k-', 'LineWidth', 1.5);
    text(0.35*prctile(ClusterRadii, 99), 0.4, Fit1_text,'HorizontalAlignment','Left', 'FontSize',7, 'FontName', 'Helvetica');
    xlabel('cluster radius (nm)', 'FontSize',9, 'FontName', 'Helvetica');
    ylabel('1-CDF', 'FontSize',9, 'FontName', 'Helvetica');
    title('1-CDF of CTCF cluster radii ', 'FontSize',9, 'FontName', 'Helvetica');
    legend('data', '1-exp', '2-exp', 'Location', 'NorthEast');
    axis([0 prctile(ClusterRadii, 99.5) 0.005 1.01]);
    set(gca,'yscale','log');
    hold off;

    
    set(figure1,'Units','Inches');
    pos = get(figure1,'Position');
    set(figure1,'PaperPositionMode','Auto','PaperUnits','Inches','PaperSize',[pos(3), pos(4)]);
    print(figure1,[EffInputPath, Workspaces{WorkIter}, '_BayesClusterSummary.pdf'],'-dpdf','-r0');
        
    %print([EffInputPath, Workspaces{WorkIter}, '_BayesClusterSummary.eps'], '-depsc', '-r300');
    %img = getframe(gcf);
    %imwrite(img.cdata, [EffInputPath, Workspaces{WorkIter}, '_BayesClusterSummary.png'], 'Resolution', 300);
    %imwrite(img.cdata, [EffInputPath, Workspaces{WorkIter}, '_BayesClusterSummary.png']);






    %%%%%%%%% OK, append the new square information to the MATLAB workspace
    if save_append_val == 1
        %first load the workspace:
        load([EffInputPath, Workspaces{WorkIter}, '.mat']);
        % then save with the new square information appended
        save([EffInputPath, Workspaces{WorkIter}, '.mat'], 'nuclear_PALM_locs', 'TrackedLocs', 'SegmentedPolygonROI', 'DriftStruct', ...
                    'MatName', 'Square_parameters', 'ParamStruct', 'XY_LocErrors', 'LocsPerFrame', 'LocLifetime', 'AvgLocError', ...
            'r_vs_T_cell', 'r_vector', 'T_vector', ...
            'ClusteredSquareParticles_local', 'ClusteredSquareParticles_global', 'global_r_T', 'mean_r_vs_T', 'best_r_vs_T', 'mean_NumberOfClusters', 'mean_NumberInCluster',...
            'mean_PercentInCluster', 'mean_ClusterRadius', 'ParsPerCluster', 'ClusterRadii');
    end  
    
    
end
